package com.tom.trainerapi.healthcheck

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(HealthCheckController::class)
@AutoConfigureMockMvc
class HealthCheckControllerTest {

    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun `simple check returns 200 OK when the service is up`() {
        mockMvc.perform(
            MockMvcRequestBuilders
                .get("/api/health/check")
                .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk)
    }
}