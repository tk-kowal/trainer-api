package com.tom.trainerapi.trainers

import com.fasterxml.jackson.databind.ObjectMapper
import org.hamcrest.Matchers.matchesPattern
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import javax.transaction.Transactional

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
class TrainersApiIntegrationTest {
    @Autowired
    lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var trainerRepo: TrainerRepository

    @Test
    fun `create trainer success returns a 201 created`() {
        val expectedEmail = "dwight@example.com"
        val expectedPhone = "(312)-555-1234"
        val expectedFirstName = "Dwight"
        val expectedLastName = "Schrute"

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/api/trainer")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapper().writeValueAsString(CreateTrainerRequest(
                    email = expectedEmail,
                    phone = expectedPhone,
                    firstName = expectedFirstName,
                    lastName = expectedLastName
                )))
        ).andExpect(
            status().isCreated
        ).andExpect(
            header().string("Location", matchesPattern("http://localhost:80/api/trainer/\\d+"))
        )
    }

    @Test
    fun `create trainer success includes top-level type`() {
        val expectedType = "trainer"

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/api/trainer")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapper().writeValueAsString(CreateTrainerRequest(
                    email = "email@example.com",
                    phone = "123-123-1234",
                    firstName = "Michael",
                    lastName = "Scott"
                )))
        ).andExpect(
            jsonPath("$.data.type").value(expectedType)
        )
    }

    @Test
    fun `create trainer success includes trainer attributes`() {
        val expectedEmail = "dwight@example.com"
        val expectedPhone = "(312)-555-1234"
        val expectedPhoneCleaned = "3125551234"
        val expectedFirstName = "Dwight"
        val expectedLastName = "Schrute"

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/api/trainer")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapper().writeValueAsString(CreateTrainerRequest(
                    email = expectedEmail,
                    phone = expectedPhone,
                    firstName = expectedFirstName,
                    lastName = expectedLastName
                )))
        ).andExpect(
            jsonPath("$.data.attributes.email").value(expectedEmail)
        ).andExpect(
            jsonPath("$.data.attributes.phone").value(expectedPhoneCleaned)
        ).andExpect(
            jsonPath("$.data.attributes.firstName").value(expectedFirstName)
        ).andExpect(
            jsonPath("$.data.attributes.lastName").value(expectedLastName)
        )
    }

    @Test
    fun `create trainer conflict returns 409 when email is in conflict`() {
        val duplicateEmail = "dwight@example.com"
        trainerRepo.save(
            Trainer(duplicateEmail, "1231231234", "Bob", "Vance")
        )

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/api/trainer")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapper().writeValueAsString(CreateTrainerRequest(
                    email = duplicateEmail,
                    phone = "8883331234",
                    firstName = "Meredith",
                    lastName = "Palmer"
                )))
        ).andExpect(status().isConflict)
    }

    @Test
    fun `create trainer conflict returns 409 when phone is in conflict`() {
        val duplicatePhone = "3334448888"
        trainerRepo.save(
            Trainer("bob@example.com", duplicatePhone, "Bob", "Vance")
        )

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/api/trainer")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapper().writeValueAsString(CreateTrainerRequest(
                    email = "meredith@example.com",
                    phone = duplicatePhone,
                    firstName = "Meredith",
                    lastName = "Palmer"
                )))
        ).andExpect(status().isConflict)
    }

    @Test
    fun `create trainer invalid returns 400 when phone is invalid`() {
        val invalidPhone = ""

        mockMvc.perform(
            MockMvcRequestBuilders
                .post("/api/trainer")
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(ObjectMapper().writeValueAsString(CreateTrainerRequest(
                    email = "meredith@example.com",
                    phone = invalidPhone,
                    firstName = "Meredith",
                    lastName = "Palmer"
                )))
        ).andExpect(status().isBadRequest)
    }

    @Test
    fun `get trainer returns returns 404 when no trainer is found with that id`() {
        val nonExistentId = 0

        mockMvc.perform(
            MockMvcRequestBuilders
                .get("/api/trainer/{id}", nonExistentId)
                .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isNotFound)
    }

    @Test
    fun `get trainer returns returns 200 and data when a trainer is found with that id`() {
        val expectedEmail = "dwight@example.com"
        val expectedPhoneCleaned = "3125551234"
        val expectedFirstName = "Dwight"
        val expectedLastName = "Schrute"

        val expectedId = trainerRepo.save(
            Trainer(expectedEmail, expectedPhoneCleaned, expectedFirstName, expectedLastName)
        ).id

        mockMvc.perform(
            MockMvcRequestBuilders
                .get("/api/trainer/{id}", expectedId)
                .accept(MediaType.APPLICATION_JSON_VALUE)
        ).andExpect(status().isOk)
            .andExpect(
                jsonPath("$.data.attributes.email").value(expectedEmail)
            ).andExpect(
                jsonPath("$.data.attributes.phone").value(expectedPhoneCleaned)
            ).andExpect(
                jsonPath("$.data.attributes.firstName").value(expectedFirstName)
            ).andExpect(
                jsonPath("$.data.attributes.lastName").value(expectedLastName)
            )
    }
}