package com.tom.trainerapi.trainers

import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import strikt.api.expectCatching
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import strikt.assertions.isFailure
import strikt.assertions.message
import java.lang.IllegalArgumentException

class TrainerServiceTest {
    lateinit var mockTrainerRepo: TrainerRepository
    lateinit var subject: TrainerService

    @BeforeEach
    fun setup() {
        mockTrainerRepo = mockk()
        subject = TrainerService(mockTrainerRepo)
    }

    @Test
    fun `createTrainer throws IllegalArgumentException when email does not match regex`() {
        val invalidEmail = "this-is-not-a-valid-email"

        expectCatching {
            subject.createTrainer(invalidEmail, "4445551234", "Andy", "Bernard")
        }.isFailure()
            .isA<IllegalArgumentException>()
    }

    @Test
    fun `createTrainer throws IllegalArgumentException when email is blank`() {
        val invalidEmail = ""

        expectCatching {
            subject.createTrainer(invalidEmail, "4445551234", "Andy", "Bernard")
        }.isFailure()
            .isA<IllegalArgumentException>()
            .and { message.isEqualTo("email is invalid.") }
    }

    @Test
    fun `createTrainer throws IllegalArgumentException when phone is shorter than 10 digits`() {
        val invalidPhone = "123456789"

        expectCatching {
            subject.createTrainer("valid@example.com", invalidPhone, "Andy", "Bernard")
        }.isFailure()
            .isA<IllegalArgumentException>()
            .and { message.isEqualTo("phone is invalid.") }
    }

    @Test
    fun `createTrainer throws IllegalArgumentException when phone is blank`() {
        val invalidPhone = ""

        expectCatching {
            subject.createTrainer("valid@example.com", invalidPhone, "Andy", "Bernard")
        }.isFailure()
            .isA<IllegalArgumentException>()
            .and { message.isEqualTo("phone is invalid.") }
    }
}