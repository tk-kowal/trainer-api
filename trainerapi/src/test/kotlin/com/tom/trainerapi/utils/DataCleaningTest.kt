package com.tom.trainerapi.utils

import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class DataCleaningTest {
    @Test
    fun `removeNonNumerics returns the original string if there are no non-numerics`() {
        expectThat(removeNonNumerics("12345")).isEqualTo("12345")
    }

    @Test
    fun `removeNonNumerics removes any non numeric characters from the string`() {
        expectThat(removeNonNumerics("(123) 444 - 1234")).isEqualTo("1234441234")
    }

    @Test
    fun `removeNonNumerics handles empty strings`() {
        expectThat(removeNonNumerics("")).isEqualTo("")
    }
}