package com.tom.trainerapi.utils

fun removeNonNumerics(input: String): String {
    return Regex("[^0-9]").replace(input, "")
}
