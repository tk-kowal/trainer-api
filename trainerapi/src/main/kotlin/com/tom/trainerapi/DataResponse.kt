package com.tom.trainerapi

class DataResponse<T>(private val entity: T) {
    val data get() = Data<T>(type = entity!!::class.java.simpleName.toLowerCase(), entity)
}

data class Data<T>(val type: String, val attributes: T)