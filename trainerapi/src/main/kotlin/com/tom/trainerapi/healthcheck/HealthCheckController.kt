package com.tom.trainerapi.healthcheck

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(path=["/api/health"])
class HealthCheckController {

    @GetMapping("/check", produces = [APPLICATION_JSON_VALUE])
    fun simpleCheck(): ResponseEntity<Void> {
        return ResponseEntity.ok().build()
    }

}