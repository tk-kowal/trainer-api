package com.tom.trainerapi.trainers

import com.tom.trainerapi.utils.removeNonNumerics
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.IllegalArgumentException

@Service
class TrainerService(@Autowired val trainerRepo: TrainerRepository) {
    fun createTrainer(email: String, phone: String, firstName: String, lastName: String? = null): Trainer {
        if (emailInvalid(email)) {
            throw IllegalArgumentException("email is invalid.")
        }

        if (phoneInvalid(phone)) {
            throw IllegalArgumentException("phone is invalid.")
        }

        return trainerRepo.save(Trainer(email, removeNonNumerics(phone), firstName, lastName))
    }

    fun emailInvalid(email: String): Boolean {
        // Regex taken from https://www.emailregex.com
        return !Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
            .matches(email)
    }

    fun phoneInvalid(phone: String): Boolean {
        return removeNonNumerics(phone).length < 10
    }

    fun findById(id: Long): Trainer? {
        return trainerRepo
            .findById(id)
            .orElse(null)
    }
}