package com.tom.trainerapi.trainers

import com.tom.trainerapi.DataResponse
import org.hibernate.exception.ConstraintViolationException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.lang.IllegalArgumentException
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping(path=["/api"])
class TrainersController(
    @Autowired val trainerService: TrainerService
) {
    @PostMapping("/trainer",
        produces = [APPLICATION_JSON_VALUE],
        consumes = [APPLICATION_JSON_VALUE])
    fun createTrainer(
        @RequestBody trainerRequest: CreateTrainerRequest,
        request: HttpServletRequest
    ): ResponseEntity<DataResponse<Trainer>> {
        try {
            val trainer = trainerService.createTrainer(
                trainerRequest.email,
                trainerRequest.phone,
                trainerRequest.firstName,
                trainerRequest.lastName)
            return ResponseEntity
                .created(ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .port(request.serverPort)
                    .path("/{id}")
                    .buildAndExpand(trainer.id)
                    .toUri())
                .body(DataResponse(trainer))
        } catch (e: DataIntegrityViolationException) {
            return when (e.cause!!::class.java) {
                ConstraintViolationException::class.java -> ResponseEntity.status(HttpStatus.CONFLICT).build()
                else -> ResponseEntity.internalServerError().build()
            }
        } catch (e: IllegalArgumentException) {
            return ResponseEntity.badRequest().build()
        }
    }

    @GetMapping("/trainer/{id}", produces = [APPLICATION_JSON_VALUE])
    fun getTrainer(
        @PathVariable id: Long
    ): ResponseEntity<DataResponse<Trainer>> {
        val trainer = trainerService.findById(id)
        return when {
            trainer != null -> ResponseEntity.ok(DataResponse(trainer))
            else -> ResponseEntity.notFound().build()
        }
    }
}