package com.tom.trainerapi.trainers

import javax.persistence.*

@Entity
@Table(name = "trainers")
class Trainer(
    val email: String,
    val phone: String,
    val firstName: String,
    val lastName: String? = null,
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = null
)