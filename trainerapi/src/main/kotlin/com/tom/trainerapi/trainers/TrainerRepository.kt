package com.tom.trainerapi.trainers

import org.springframework.data.repository.CrudRepository

interface TrainerRepository : CrudRepository<Trainer, Long> {
    fun findByEmail(email: String): Trainer?
}