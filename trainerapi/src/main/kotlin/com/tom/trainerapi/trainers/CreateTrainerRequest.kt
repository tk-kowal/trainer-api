package com.tom.trainerapi.trainers

data class CreateTrainerRequest(val email: String,
                                val phone: String,
                                val firstName: String,
                                val lastName: String?)