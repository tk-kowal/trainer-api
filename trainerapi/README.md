# Trainer API

This RESTful API, a [Spring Boot](https://spring.io/projects/spring-boot) application written in [Kotlin](https://kotlinlang.org), allows users to create Trainers and look them up by id.
It draws on conventions defined in the [JSON API Spec](https://jsonapi.org).
With continued development it could be made compliant.

## Setup

### Install Postgres
If you don't already have postgres set up, you can install it with brew
`brew install postgres` 

You can make use of brew services to run/stop/restart postgres with:

```
brew services start postgres
brew services stop postgres
brew services restart postgres
```

### Create Databases
Run the following from the project root to create the dev and test databases
and the api user with the expected password.

`psql -f ./src/main/resources/db/init/create_db.sql`

This will create `trainerapi` and `trainerapi-test` databases as well as a
`api` role with password `hey-there-folks!` This is intended to keep local
setup simple and not intended for deployed environments where credentials would
be better protected.

---

## Testing

To run integration tests which bring up the full Spring Boot application and hit
the test database, run:

`./gradlew integrationTest`

To run unit tests which are faster and do not bring up the full application context 
or hit the database, run:

`./gradlew unitTest`

To run all tests, run:

`./gradlew test`

---

## Running the API

To run the application locally, run:

`./gradlew bootRun`

> Note: Make sure you've created the databases with the script above before running the application

### Postman

At the root of the project you'll find a [Postman](https://www.postman.com) collection
which you can import into Postman to try out the API.

To import the collection, open Postman then `File > Import` and select the json file.

---

## API Design

The trainer schema is as follows:

```
    "id": Long (auto-incrementing)
    "email": String (required, must be unique)
    "phone": String (required, must be unique)
    "firstName": String (required)
    "lastName": String (optional)
```

> Note: lastName was made optional to accommodate folks who don't have two names. 
> [A more robust solution would be to store a single name field.](https://www.w3.org/International/questions/qa-personal-names#singlefield)

`GET /api/health/check` which just returns a 200 if the application is up

`POST /api/trainer` which creates a trainer given a request body like:

```json
{
    "email": "bob@example.com",
    "phone": "7735550881",
    "firstName": "Bob",
    "lastName": "Odenkirk"
}
```

If a trainer with the same email or phone already exists, the API will return a 409 Conflict.

`GET /api/trainer/{id}` which will return a trainer for the given id if one exists
otherwise it will return a 404.

---

## Next Steps

If I had more time to work on this I would:

1. Improve error handling to return more information about the error. For example,
on conflicts I would return the fields that caused the insert to fail.
   
2. I would look into reducing the friction around local database setup by potentially
containerizing the application and postgres which could then be spun up with docker compose.
   
3. I would expand on the API by adding support for:

   - Updating & Deleting trainer records
   - Searching for trainers by email, phone, name
   - Bringing the API up to spec with the [JSON API Specification](https://jsonapi.org)