import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.5.3"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("com.adarshr.test-logger") version "3.0.0"
    id("org.flywaydb.flyway") version "7.3.1"
	id("org.jetbrains.kotlin.plugin.jpa") version "1.5.21"
	kotlin("jvm") version "1.5.21"
	kotlin("plugin.spring") version "1.5.21"
}

group = "com.tom"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.flywaydb:flyway-core:7.12.0")
    implementation("org.postgresql:postgresql:42.2.16")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa:2.5.3")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.strikt:strikt-core:0.31.0")
	testImplementation("io.mockk:mockk:1.12.0")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

flyway {
	url = "jdbc:postgresql://localhost:5432/trainerapi"
    user = "api"
	password = "hey-there-folks!"
}

task<Test>("unitTest") {
	exclude("**/*IntegrationTest*")
}

task<Test>("integrationTest") {
	include("**/*IntegrationTest*")
}
